//
//  ClockView.h
//  memory
//
//  Created by Tuan Phung on 4/6/14.
//  Copyright (c) 2014 Tuan Phung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimelineView : UIView {
    NSTimeInterval _interval;
    double _duration;
    double _percentPerInterval;
    double _percent;
    NSTimer *_timer;
}

- (void) startCountDownWithDuration:(double)duration;
- (void) descreasePercent:(double)percent;

- (double) percent;
- (void) reset;

@end
