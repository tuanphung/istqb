//
//  ChapterSectionInfo.h
//  istqb
//
//  Created by Tuan Phung on 5/25/14.
//  Copyright (c) 2014 Tuan Phung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Chapter.h"
#import "ChapterSectionHeaderView.h"

@interface ChapterSectionInfo : NSObject

@property (nonatomic) BOOL expended;
@property (nonatomic) NSInteger section;
@property (retain, nonatomic) Chapter *chapter;
@property (retain, nonatomic) ChapterSectionHeaderView *sectionHeaderView;

@end
