//
//  ChapterHeaderView.h
//  istqb
//
//  Created by Tuan Phung on 5/25/14.
//  Copyright (c) 2014 Tuan Phung. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChapterSectionHeaderView;

@protocol ChapterSectionHeaderViewDelegate <NSObject>

@optional
-(void)sectionHeaderView:(ChapterSectionHeaderView*)sectionHeaderView sectionOpened:(NSInteger)section;
-(void)sectionHeaderView:(ChapterSectionHeaderView*)sectionHeaderView sectionClosed:(NSInteger)section;

@end


@interface ChapterSectionHeaderView : UIView

@property (nonatomic) NSInteger section;
@property (retain, nonatomic) UIImageView *leftIconView;
@property (retain, nonatomic) UILabel *titleLabel;
@property (retain, nonatomic) UILabel *decriptionLabel;
@property (retain, nonatomic) UIImageView *rightIconView;
@property (assign, nonatomic) id<ChapterSectionHeaderViewDelegate> delegate;

- (void) initialize;
- (CGFloat) height;

@end
