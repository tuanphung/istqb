//
//  ChapterHeaderView.m
//  istqb
//
//  Created by Tuan Phung on 5/25/14.
//  Copyright (c) 2014 Tuan Phung. All rights reserved.
//

#define ChapterSectionHeaderViewHeight    60

#import "ChapterSectionHeaderView.h"

@interface ChapterSectionHeaderView(){
    BOOL _expanded;
}
@end

@implementation ChapterSectionHeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initialize];
    }
    return self;
}

- (void) initialize
{
    _expanded = NO;
    
    /*
    self.leftIconView = [[UIImageView alloc] init];
    self.leftIconView.frame = CGRectMake(10, (ChapterSectionHeaderViewHeight - 40)/2, 40, 40);
    [self addSubview:self.leftIconView];
    */
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.bounds.size.width - 90, 25)];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
    self.titleLabel.textColor = RGBA(14, 153, 223, 1);
    [self addSubview:self.titleLabel];
    
    self.decriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 35, self.bounds.size.width - 90, 15)];
    self.decriptionLabel.backgroundColor = [UIColor clearColor];
    self.decriptionLabel.font = [UIFont fontWithName:@"Helvetica" size:13];
    self.decriptionLabel.textColor = [UIColor darkGrayColor];
    [self addSubview:self.decriptionLabel];
    
    self.rightIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_collapsed"]];
    self.rightIconView.frame = CGRectMake(self.bounds.size.width - 30, (ChapterSectionHeaderViewHeight - 15)/2, 15, 15);
    [self addSubview:self.rightIconView];
    
    self.backgroundColor = RGBA(233, 239, 244, 1);
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleOpen:)];
    [self addGestureRecognizer:tapGesture];
}

- (void) toggleOpen:(UITapGestureRecognizer*)tapGesture
{
    _expanded = !_expanded;
    if (_expanded) {
        self.rightIconView.image = [UIImage imageNamed:@"arrow_expanded"];
        if ([self.delegate respondsToSelector:@selector(sectionHeaderView:sectionOpened:)]) {
            [self.delegate sectionHeaderView:self sectionOpened:self.section];
        }
    }
    else {
        self.rightIconView.image = [UIImage imageNamed:@"arrow_collapsed"];
        if ([self.delegate respondsToSelector:@selector(sectionHeaderView:sectionClosed:)]) {
            [self.delegate sectionHeaderView:self sectionClosed:self.section];
        }
    }
}

- (CGFloat) height
{
    return ChapterSectionHeaderViewHeight;
}

- (void) drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    if (self.section == 0) return;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(context, [[UIColor whiteColor] CGColor]);
    CGContextSetLineWidth(context, 2);
    
    CGContextMoveToPoint(context, 0, 0);
    CGContextAddLineToPoint(context, rect.size.width, 0);
    
    CGContextDrawPath(context, kCGPathFillStroke);
}

@end
