//
//  ClockView.m
//  memory
//
//  Created by Tuan Phung on 4/6/14.
//  Copyright (c) 2014 Tuan Phung. All rights reserved.
//

#import "TimelineView.h"

@implementation TimelineView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        _timer = nil;
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void) startCountDownWithDuration:(double)duration {
    _interval = 0.02f;
    _duration = duration;
    _percentPerInterval = _interval / _duration * 100;
    _percent = 0;
    
    [self setNeedsDisplay];
    
    [_timer invalidate];
    _timer = [NSTimer scheduledTimerWithTimeInterval:_interval target:self selector:@selector(countdown) userInfo:nil repeats:YES];
}

- (void) descreasePercent:(double)percent {
    _percent = percent + _percent;
    
    if (_percent >= 100) {
        _percent = 100;
        [self setNeedsDisplay];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TimeUp" object:nil];
        
        [_timer invalidate];
        _timer = nil;
    }
}

- (double) percent {
    return _percent;
}

- (void) reset {
    _percent = 0;
    [self setNeedsDisplay];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextMoveToPoint(ctx, rect.size.width/2, rect.size.height/2);
    CGContextSetStrokeColorWithColor(ctx, [[UIColor colorWithRed:95.0f/255.0f green:81.0f/255.0f blue:78.0f/255.0f alpha:1.0f] CGColor]);
    CGContextSetFillColorWithColor(ctx, [[UIColor colorWithRed:95.0f/255.0f green:81.0f/255.0f blue:78.0f/255.0f alpha:0.7f] CGColor]);
    CGContextAddEllipseInRect(ctx, rect);
    CGContextDrawPath(ctx, kCGPathFillStroke);

}


- (void) countdown
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TimeTick" object:nil];
    if (_percent >= 100) {
        _percent = 100;
        [self setNeedsDisplay];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TimeUp" object:nil];
        
        [_timer invalidate];
        _timer = nil;
    }
    else {
        _percent += _percentPerInterval;
        [self setNeedsDisplay];
    }
}

@end
