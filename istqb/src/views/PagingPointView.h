//
//  PagingPointView.h
//  hotelbooking
//
//  Created by YellowPepper VietNam on 11/20/13.
//  Copyright (c) 2013 yellowpepper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface PagingPointView : UIView {
    int _numberOfPages;
    int _currentIndex;
}

@property int numberOfPages;
@property (retain) CAShapeLayer *pagingPointLayer;
@property (retain) NSMutableArray *pointLayers;

- (void) initialize;
- (void) updatePageIndex:(int)index;
@end
