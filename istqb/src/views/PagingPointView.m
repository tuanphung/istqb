//
//  PagingPointView.m
//  hotelbooking
//
//  Created by YellowPepper VietNam on 11/20/13.
//  Copyright (c) 2013 yellowpepper. All rights reserved.
//

#import "PagingPointView.h"

@implementation PagingPointView
@dynamic numberOfPages;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        [self initialize];
    }
    return self;
}

- (void) initialize
{
    self.pagingPointLayer = [CAShapeLayer layer];
    self.pagingPointLayer.bounds = CGRectMake(0, 0, self.bounds.size.width, 20);
    self.pagingPointLayer.position = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMaxY(self.bounds) - 10);
    
    [self.layer addSublayer:self.pagingPointLayer];
    
    self.pointLayers = [[NSMutableArray alloc] init];
}

- (int) numberOfPages
{
    return _numberOfPages;
}

- (void) setNumberOfPages:(int)numberOfPages
{
    _numberOfPages = numberOfPages;
    
    for (CALayer *layer in self.pagingPointLayer.sublayers) {
        [layer removeFromSuperlayer];
    }
    [self.pointLayers removeAllObjects];
    
    int startOffsetX = (self.bounds.size.width - (self.numberOfPages * 20))/2;
    _currentIndex = 0;
    for (int i = 0; i < self.numberOfPages; i++) {
        CAShapeLayer *pointLayer = [CAShapeLayer layer];
        
        pointLayer.bounds = CGRectMake(0, 0, 20, 20);
        pointLayer.position = CGPointMake(i*20 + startOffsetX + 10, 10);
        pointLayer.strokeColor = [[UIColor lightGrayColor] CGColor];
        pointLayer.fillColor = (i == 0) ? [[UIColor lightGrayColor] CGColor] : [[UIColor clearColor] CGColor];
        
        CGMutablePathRef pointPath = CGPathCreateMutable();
        CGPathAddEllipseInRect(pointPath, nil, CGRectMake(5, 5, 10, 10));
        pointLayer.path = pointPath;
        
        [self.pagingPointLayer addSublayer:pointLayer];
        [self.pointLayers addObject:pointLayer];
    }
}

- (void) updatePageIndex:(int)index
{
    CAShapeLayer *activePointLayer = [self.pointLayers objectAtIndex:_currentIndex];
    activePointLayer.fillColor = [[UIColor clearColor] CGColor];
    CGMutablePathRef pointPath = CGPathCreateMutable();
    CGPathAddEllipseInRect(pointPath, nil, CGRectMake(5, 5, 10, 10));
    activePointLayer.path = pointPath;
    
    CAShapeLayer *pointLayer = [self.pointLayers objectAtIndex:index];
    pointLayer.fillColor = [[UIColor lightGrayColor] CGColor];
    pointPath = CGPathCreateMutable();
    CGPathAddEllipseInRect(pointPath, nil, CGRectMake(5, 5, 10, 10));
    pointLayer.path = pointPath;
    
    _currentIndex = index;
}

@end
