//
//  NSString+Extension.m
//  myplaces
//
//  Created by YellowPepper VietNam on 8/23/13.
//  Copyright (c) 2013 P&T. All rights reserved.
//

#import "NSString+Extension.h"

@implementation NSString (NSString_Extension)

- (NSString*) stringByReplacingWithReplacements:(NSDictionary*)replacements
{
    NSString *outputString = self;
    for (NSString *key in replacements) {
        outputString = [outputString stringByReplacingOccurrencesOfString:key withString:[replacements objectForKey:key]];
    }
    return outputString;
}
@end
