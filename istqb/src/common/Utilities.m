//
//  Utilities.m
//  mymoney
//
//  Created by YellowPepper VietNam on 7/5/13.
//  Copyright (c) 2013 P&T. All rights reserved.
//

#import "Utilities.h"

@implementation Utilities

+ (NSString*) randomString:(int)length
{
    NSMutableString *uid = [[NSMutableString alloc] init];
    NSString *characterSet = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
    for (int i = 0; i < length; i++) {
        int randomNumber = arc4random();
        int index = randomNumber % characterSet.length;
        [uid appendString:[characterSet substringWithRange:NSMakeRange(index, 1)]];
    }
    return uid;
}

+ (UIImage*) captureView:(UIView *)view inRect:(CGRect)rect
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
        UIGraphicsBeginImageContextWithOptions([view bounds].size, NO, [UIScreen mainScreen].scale);
    else
        UIGraphicsBeginImageContext([view bounds].size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    [view.layer renderInContext:context];
    UIImage *screenshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
        rect = CGRectMake(rect.origin.x * 2, rect.origin.y * 2, rect.size.width * 2, rect.size.height * 2);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([screenshot CGImage], rect);
    UIImage *croppedScreenshot = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return croppedScreenshot;
}
@end
