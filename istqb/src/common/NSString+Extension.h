//
//  NSString+Extension.h
//  myplaces
//
//  Created by YellowPepper VietNam on 8/23/13.
//  Copyright (c) 2013 P&T. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NSString_Extension)

- (NSString*) stringByReplacingWithReplacements:(NSDictionary*)replacements;

@end
