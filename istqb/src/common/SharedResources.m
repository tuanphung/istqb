//
//  SharedResources.m
//  myplaces
//
//  Created by YellowPepper VietNam on 7/11/13.
//  Copyright (c) 2013 P&T. All rights reserved.
//

#import "SharedResources.h"

static SharedResources *instance = nil;

@interface SharedResources() {
@private
    NSMutableDictionary *data;
    dispatch_queue_t queue;
}

+ (SharedResources*) getInstance;

- (void) removeAllResources;
- (void) setValue:(id)value forKey:(NSString*)key;
- (id) valueForKey:(NSString*)key;

@end

@implementation SharedResources

- (id) init
{
    self = [super init];
    if (self) {
        data = [[NSMutableDictionary alloc] init];
        queue = dispatch_queue_create("sharedResources_queue", NULL);
    }
    return self;
}

- (void) removeAllResources
{
    dispatch_sync(queue, ^{
        [data removeAllObjects];
    });
}

- (void) setValue:(id)value forKey:(NSString*)key
{
    dispatch_sync(queue, ^{
        [data setValue:value forKey:key];
    });
}

- (id) valueForKey:(NSString*)key
{
    __block id value = nil;
    dispatch_sync(queue, ^{
        value = [data objectForKey:key];
    });
    return value;
}

#pragma Static functions
+ (SharedResources*) getInstance
{
    //initialize instance to use
    if (instance == nil) {
        instance = [[SharedResources alloc] init];
    }
    
    return instance;
}

+ (void) removeAllResources
{
    [[SharedResources getInstance] removeAllResources];
}

+ (void) setValue:(id)value forKey:(NSString*)key
{
    [[SharedResources getInstance] setValue:value forKey:key];
}

+ (id) valueForKey:(NSString*)key
{
    return [[SharedResources getInstance] valueForKey:key];
}

@end
