//
//  Utilities.h
//  mymoney
//
//  Created by YellowPepper VietNam on 7/5/13.
//  Copyright (c) 2013 P&T. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SharedResources.h"

@interface Utilities : NSObject

+ (NSString*) randomString:(int)length;
+ (UIImage*) captureView:(UIView *)view inRect:(CGRect)rect;

@end
