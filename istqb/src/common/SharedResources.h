//
//  SharedResources.h
//  myplaces
//
//  Created by YellowPepper VietNam on 7/11/13.
//  Copyright (c) 2013 P&T. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SharedResources : NSObject

+ (void) removeAllResources;
+ (void) setValue:(id)value forKey:(NSString*)key;
+ (id) valueForKey:(NSString*)key;

@end
