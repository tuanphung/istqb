//
//  QuizViewController.m
//  istqb
//
//  Created by Tuan Phung on 5/25/14.
//  Copyright (c) 2014 Tuan Phung. All rights reserved.
//

#import "QuizViewController.h"
#import "QuizDetailViewController.h"

@interface QuizViewController ()

@end

@implementation QuizViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self parseJSON];
    [self.chaptersTableView reloadData];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    
    self.tabBarController.title = @"Quiz";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) parseJSON {
    NSData *jsonData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"data" ofType:@"json"]];
    NSJSONSerialization *json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:nil];
    
    self.chapters = [NSMutableArray arrayWithCapacity:0];
    self.chapterSectionInfos = [NSMutableArray arrayWithCapacity:0];
    
    NSArray *chaptersData = [json valueForKey:@"chapters"];
    int i = 0;
    for (NSDictionary *chapterData in chaptersData) {
        Chapter *chapter = [[Chapter alloc] initWithJON:chapterData];
        [self.chapters addObject:chapter];
        
        ChapterSectionInfo *sectionInfo = [[ChapterSectionInfo alloc] init];
        sectionInfo.chapter = chapter;
        sectionInfo.expended = NO;
        sectionInfo.section = i;
        
        ChapterSectionHeaderView *sectionHeaderView = [[ChapterSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 40)];
        sectionHeaderView.section = i;
        sectionHeaderView.delegate = self;
        sectionHeaderView.titleLabel.text = [NSString stringWithFormat:@"%@", chapter.title];
        sectionHeaderView.decriptionLabel.text = [NSString stringWithFormat:@"%@", chapter.desc];
        
        sectionInfo.sectionHeaderView = sectionHeaderView;
        
        [self.chapterSectionInfos addObject:sectionInfo];
        i++;
    }
}

#pragma UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.chapterSectionInfos.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    ChapterSectionInfo *sectionInfo = [self.chapterSectionInfos objectAtIndex:section];
    if ([sectionInfo expended]) {
        return sectionInfo.chapter.subChapters.count;
    }
    else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"UITableViewCell";
    
    UITableViewCell *cell = nil;
    
    //reuse cell
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    //init cell
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    ChapterSectionInfo *sectionInfo = [self.chapterSectionInfos objectAtIndex:indexPath.section];
    SubChapter *subChapter = [sectionInfo.chapter.subChapters objectAtIndex:indexPath.row];
    cell.textLabel.text = subChapter.title;
    cell.textLabel.textColor = RGBA(66, 139, 202, 1);
    cell.detailTextLabel.text = subChapter.desc;

    return cell;
}

#pragma UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    ChapterSectionHeaderView *sectionHeaderView = [[self.chapterSectionInfos objectAtIndex:section] sectionHeaderView];
    return [sectionHeaderView height];
}

- (UIView*) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[self.chapterSectionInfos objectAtIndex:section] sectionHeaderView];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChapterSectionInfo *sectionInfo = [self.chapterSectionInfos objectAtIndex:indexPath.section];
    SubChapter *subChapter = [sectionInfo.chapter.subChapters objectAtIndex:indexPath.row];
    
    QuizDetailViewController *quizDetailViewController = [[QuizDetailViewController alloc] initWithNibName:@"QuizDetailViewController" bundle:nil];
    quizDetailViewController.subChapter = subChapter;
    [self.navigationController pushViewController:quizDetailViewController animated:YES];
}

#pragma SectionHeaderViewDelegate
- (void) sectionHeaderView:(ChapterSectionHeaderView *)sectionHeaderView sectionOpened:(NSInteger)section
{
    [self.chaptersTableView beginUpdates];
    [[self.chapterSectionInfos objectAtIndex:section] setExpended:YES];
    ChapterSectionInfo *sectionInfo = [self.chapterSectionInfos objectAtIndex:section];
    NSMutableArray *indexPathsForInsert = [NSMutableArray arrayWithCapacity:0];
    for (int i = 0; i < sectionInfo.chapter.subChapters.count; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:section];
        [indexPathsForInsert addObject:indexPath];
    }
    [self.chaptersTableView insertRowsAtIndexPaths:indexPathsForInsert withRowAnimation:UITableViewRowAnimationBottom];
    NSLog(@"Open %d rows", indexPathsForInsert.count);
    [self.chaptersTableView endUpdates];
}

- (void) sectionHeaderView:(ChapterSectionHeaderView *)sectionHeaderView sectionClosed:(NSInteger)section
{
    [self.chaptersTableView beginUpdates];
    [[self.chapterSectionInfos objectAtIndex:section] setExpended:NO];
    
    ChapterSectionInfo *sectionInfo = [self.chapterSectionInfos objectAtIndex:section];
    NSMutableArray *indexPathsForDelete = [NSMutableArray arrayWithCapacity:0];
    for (int i = 0; i < sectionInfo.chapter.subChapters.count; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:section];
        [indexPathsForDelete addObject:indexPath];
    }
    [self.chaptersTableView deleteRowsAtIndexPaths:indexPathsForDelete withRowAnimation:UITableViewRowAnimationTop];
    NSLog(@"CLose %d rows", indexPathsForDelete.count);
    [self.chaptersTableView endUpdates];
}

#pragma UISearchBarDelegate Implementation
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self.searchBarView setShowsCancelButton:YES animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self.searchBarView setShowsCancelButton:NO animated:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSLog(@"searchBarSearchButtonClicked");
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    NSLog(@"searchBarCancelButtonClicked");
    [searchBar resignFirstResponder];
}
@end
