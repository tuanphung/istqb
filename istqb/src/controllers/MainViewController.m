//
//  MainViewController.m
//  istqb
//
//  Created by Tuan Phung on 5/25/14.
//  Copyright (c) 2014 Tuan Phung. All rights reserved.
//

#import "MainViewController.h"

#import "HomeViewController.h"
#import "QuizViewController.h"
#import "RecentViewController.h"
#import "MoreViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    HomeViewController *homeViewController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    homeViewController.tabBarItem.image = [UIImage imageNamed:@"icon-home"];
    homeViewController.title = @"Home";
    
    QuizViewController *quizViewController = [[QuizViewController alloc] initWithNibName:@"QuizViewController" bundle:nil];
    quizViewController.tabBarItem.image = [UIImage imageNamed:@"icon-quiz"];
    quizViewController.title = @"Quiz";
    
    RecentViewController *recentViewController = [[RecentViewController alloc] initWithNibName:@"RecentViewController" bundle:nil];
    recentViewController.tabBarItem.image = [UIImage imageNamed:@"icon-recent"];
    recentViewController.title = @"Recent";
    
    MoreViewController *moreViewController = [[MoreViewController alloc] initWithNibName:@"MoreViewController" bundle:nil];
    moreViewController.tabBarItem.image = [UIImage imageNamed:@"icon-more"];
    moreViewController.title = @"More";
    
    self.viewControllers = @[homeViewController, quizViewController, recentViewController, moreViewController];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
