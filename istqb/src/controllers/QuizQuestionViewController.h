//
//  QuizQuestionViewController.h
//  istqb
//
//  Created by Tuan Phung on 5/25/14.
//  Copyright (c) 2014 Tuan Phung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Question.h"

@interface QuizQuestionViewController : UIViewController<UIWebViewDelegate> {
    int _currentQuestionIndex;
}

@property (strong) NSMutableArray *questions;
@property (strong, nonatomic) IBOutlet UILabel *topBarLabel;

@property (strong, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (strong, nonatomic) IBOutlet UIWebView *questionWebView;

@property (strong, nonatomic) IBOutlet UITableView *answerTableView;

- (IBAction)previousQuestion:(id)sender;
- (IBAction)nextQuestion:(id)sender;
@end
