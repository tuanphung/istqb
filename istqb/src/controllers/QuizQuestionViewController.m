//
//  QuizQuestionViewController.m
//  istqb
//
//  Created by Tuan Phung on 5/25/14.
//  Copyright (c) 2014 Tuan Phung. All rights reserved.
//

#import "QuizQuestionViewController.h"

@interface QuizQuestionViewController ()

@end

@implementation QuizQuestionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.questionWebView.delegate = self;
    
    self.answerTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Submit" style:UIBarButtonItemStylePlain target:self action:@selector(submit:)];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    _currentQuestionIndex = 0;
    [self showQuestionAtIndex:_currentQuestionIndex];
}

- (void) submit:(id)sender
{
    [[[UIAlertView alloc] initWithTitle:nil message:@"Thank you for your using!!!" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
}

- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *height = [webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight"];
    CGRect frame = self.questionWebView.frame;
    frame.size.height = [height integerValue];
    //self.questionWebView.frame = frame;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)previousQuestion:(id)sender {
    if (_currentQuestionIndex == 0) {
        return;
    }
    
    _currentQuestionIndex--;
    [self showQuestionAtIndex:_currentQuestionIndex];
}

- (IBAction)nextQuestion:(id)sender {
    if (_currentQuestionIndex == self.questions.count - 1) {
        return;
    }
    
    _currentQuestionIndex++;
    [self showQuestionAtIndex:_currentQuestionIndex];
}

- (void) showQuestionAtIndex:(int)index {
    self.topBarLabel.text = [NSString stringWithFormat:@"Question %d/%d", (_currentQuestionIndex+1), self.questions.count];
    
    Question *question = [self.questions objectAtIndex:index];
    
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    [self.questionWebView loadHTMLString:[NSString stringWithFormat:@"%@",question.detail] baseURL:baseURL];
    
    [self.answerTableView reloadData];
}

#pragma UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self.questions objectAtIndex:_currentQuestionIndex] answers] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *QuestionTableViewCell = @"DescriptionTableViewCell";
    
    UITableViewCell *cell = nil;
    
    cell = [tableView dequeueReusableCellWithIdentifier:QuestionTableViewCell];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:QuestionTableViewCell];
    }
    
    Answer *answer = [[[self.questions objectAtIndex:_currentQuestionIndex] answers] objectAtIndex:indexPath.row];
    cell.imageView.image = [UIImage imageNamed:@"uncheckmark"];
    cell.textLabel.text = answer.detail;
    cell.selectionStyle = UITableViewCellEditingStyleNone;
    return cell;
}

#pragma UITableViewDelegate
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.imageView.image = [UIImage imageNamed:@"checkmark"];
}
@end
