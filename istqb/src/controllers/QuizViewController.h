//
//  QuizViewController.h
//  istqb
//
//  Created by Tuan Phung on 5/25/14.
//  Copyright (c) 2014 Tuan Phung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Chapter.h"

#import "ChapterSectionInfo.h"
#import "ChapterSectionHeaderView.h"

@interface QuizViewController : UIViewController<UISearchBarDelegate, ChapterSectionHeaderViewDelegate>

@property (strong, nonatomic) IBOutlet UISearchBar *searchBarView;
@property (strong) NSMutableArray *chapters;
@property (strong) NSMutableArray *chapterSectionInfos;
@property (strong, nonatomic) IBOutlet UITableView *chaptersTableView;

@end
