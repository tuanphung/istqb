//
//  QuizDetailViewController.h
//  istqb
//
//  Created by Tuan Phung on 5/25/14.
//  Copyright (c) 2014 Tuan Phung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubChapter.h"

@interface QuizDetailViewController : UIViewController<UITableViewDataSource, UITableViewDelegate> {
    CGRect rect;
}

@property (strong) SubChapter *subChapter;
@property (strong, nonatomic) IBOutlet UITableView *subChapterTableView;

@end
