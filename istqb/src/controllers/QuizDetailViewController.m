//
//  QuizDetailViewController.m
//  istqb
//
//  Created by Tuan Phung on 5/25/14.
//  Copyright (c) 2014 Tuan Phung. All rights reserved.
//

#import "QuizDetailViewController.h"
#import "QuizQuestionViewController.h"

@interface QuizDetailViewController ()

@end

@implementation QuizDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.title = self.subChapter.title;
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Start" style:UIBarButtonItemStylePlain target:self action:@selector(start:)];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    rect = [self.subChapter.desc
                   boundingRectWithSize:CGSizeMake(300, 0)
                   options:NSStringDrawingUsesLineFragmentOrigin
                   attributes:@{
                                NSFontAttributeName : [UIFont systemFontOfSize:20]
                                }
                   context:nil];
}

- (void) start:(id)sender
{
    QuizQuestionViewController *quizQuestionViewController = [[QuizQuestionViewController alloc] initWithNibName:@"QuizQuestionViewController" bundle:nil];
    quizQuestionViewController.questions = self.subChapter.questions;
    quizQuestionViewController.title = self.subChapter.title;
    [self.navigationController pushViewController:quizQuestionViewController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            return 6;
            break;
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *DescriptionTableViewCell = @"DescriptionTableViewCell";
    static NSString *HistoryTableViewCell = @"HistoryTableViewCell";
    
    UITableViewCell *cell = nil;
    
    switch (indexPath.section) {
        case 0:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:DescriptionTableViewCell];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:DescriptionTableViewCell];
            }
            
            cell.textLabel.text = self.subChapter.desc;
            cell.textLabel.numberOfLines = 0;
            break;
        }
        case 1:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:HistoryTableViewCell];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:HistoryTableViewCell];
            }
            break;
        }
        default:
            break;
    }
    
    return cell;
}

#pragma UITableViewDelegate
- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return @"Infomation";
            break;
        case 1:
            return @"History";
            break;
        default:
            return nil;
            break;
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
            return rect.size.height;
            break;
        case 1:
            return 60;
            break;
        default:
            return 0;
            break;
    }
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

@end
