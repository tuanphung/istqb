//
//  Chapter.m
//  istqb
//
//  Created by Tuan Phung on 5/25/14.
//  Copyright (c) 2014 Tuan Phung. All rights reserved.
//

#import "Chapter.h"

@implementation Chapter

- (id) initWithJON:(NSDictionary*)json {
    self = [super init];
    if (self) {
        self.id = [[json valueForKey:@"id"] integerValue];
        self.title = [json valueForKey:@"title"];
        self.desc = [json valueForKey:@"description"];
        
        self.subChapters = [NSMutableArray arrayWithCapacity:0];
        NSArray *jsonSubChapters = [json valueForKey:@"subChapters"];
        for (NSDictionary *jsonSubChapter in jsonSubChapters) {
            SubChapter *subChapter = [[SubChapter alloc] initWithJON:jsonSubChapter];
            [self.subChapters addObject:subChapter];
        }
    }
    return self;
}
@end
