//
//  Answer.h
//  istqb
//
//  Created by Tuan Phung on 5/25/14.
//  Copyright (c) 2014 Tuan Phung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Answer : NSObject

@property int id;
@property (strong) NSString *detail;
@property BOOL isCorrect;

- (id) initWithJON:(NSDictionary*)json;

@end
