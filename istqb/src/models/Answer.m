//
//  Answer.m
//  istqb
//
//  Created by Tuan Phung on 5/25/14.
//  Copyright (c) 2014 Tuan Phung. All rights reserved.
//

#import "Answer.h"

@implementation Answer

- (id) initWithJON:(NSDictionary*)json {
    self = [super init];
    if (self) {
        self.id = [[json valueForKey:@"id"] integerValue];
        self.detail = [json valueForKey:@"detail"];
        self.isCorrect = [[json valueForKey:@"isCorrect"] boolValue];
    }
    return self;
}

@end
