//
//  Question.h
//  istqb
//
//  Created by Tuan Phung on 5/25/14.
//  Copyright (c) 2014 Tuan Phung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Answer.h"

@interface Question : NSObject

@property int id;
@property (strong) NSString *detail;
@property (strong) NSMutableArray *answers;

- (id) initWithJON:(NSDictionary*)json;

@end
