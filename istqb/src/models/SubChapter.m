//
//  SubChapter.m
//  istqb
//
//  Created by Tuan Phung on 5/25/14.
//  Copyright (c) 2014 Tuan Phung. All rights reserved.
//

#import "SubChapter.h"

@implementation SubChapter

- (id) initWithJON:(NSDictionary*)json {
    self = [super init];
    if (self) {
        self.id = [[json valueForKey:@"id"] integerValue];
        self.title = [json valueForKey:@"title"];
        self.desc = [json valueForKey:@"description"];
        
        self.questions = [NSMutableArray arrayWithCapacity:0];
        NSArray *jsonQuestions = [json valueForKey:@"questions"];
        for (NSDictionary *jsonQuestion in jsonQuestions) {
            Question *question = [[Question alloc] initWithJON:jsonQuestion];
            [self.questions addObject:question];
        }
    }
    return self;
}

@end
