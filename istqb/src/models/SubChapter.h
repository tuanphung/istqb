//
//  SubChapter.h
//  istqb
//
//  Created by Tuan Phung on 5/25/14.
//  Copyright (c) 2014 Tuan Phung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Question.h"

@interface SubChapter : NSObject

@property int id;
@property (strong) NSString *title;
@property (strong) NSString *desc;
@property (strong) NSMutableArray *questions;

- (id) initWithJON:(NSDictionary*)json;

@end
