//
//  Chapter.h
//  istqb
//
//  Created by Tuan Phung on 5/25/14.
//  Copyright (c) 2014 Tuan Phung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SubChapter.h"

@interface Chapter : NSObject

@property int id;
@property (strong) NSString *title;
@property (strong) NSString *desc;
@property (strong) NSMutableArray *subChapters;

- (id) initWithJON:(NSDictionary*)json;

@end
