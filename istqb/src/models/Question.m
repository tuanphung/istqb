//
//  Question.m
//  istqb
//
//  Created by Tuan Phung on 5/25/14.
//  Copyright (c) 2014 Tuan Phung. All rights reserved.
//

#import "Question.h"

@implementation Question

- (id) initWithJON:(NSDictionary*)json {
    self = [super init];
    if (self) {
        self.id = [[json valueForKey:@"id"] integerValue];
        self.detail = [json valueForKey:@"detail"];
        
        self.answers = [NSMutableArray arrayWithCapacity:0];
        NSArray *jsonAnswers = [json valueForKey:@"answers"];
        for (NSDictionary *jsonAnswer in jsonAnswers) {
            Answer *answer = [[Answer alloc] initWithJON:jsonAnswer];
            [self.answers addObject:answer];
        }
    }
    return self;
}

@end
